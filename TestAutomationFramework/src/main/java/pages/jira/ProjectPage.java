package pages.jira;

import org.openqa.selenium.WebDriver;

import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;
import static com.telerikacademy.testframework.Utils.getUIMappingByKey;

import org.openqa.selenium.support.ui.WebDriverWait;

public class ProjectPage extends BaseJiraPage {

    private WebDriverWait wait;

    public ProjectPage(WebDriver driver) {
        super(driver, "jira.projectUrl");
    }

    public void createIssue(String issueType) {

        actions.waitForElementClickable("jira.projectsPage.createButton");
        actions.clickElement("jira.projectsPage.createButton");

        actions.waitForElementVisible("jira.dropDown.selectIssueType");
        actions.clickElement("jira.dropDown.selectIssueType");
        actions.waitForElementPresent("jira.projectPage." + issueType + ".issueType");
        actions.clickElement("jira.projectPage." + issueType + ".issueType");

        actions.waitForElementVisible("jira.projectPage.summaryField");
        actions.typeValueInField(getConfigPropertyByKey("jira." + issueType + ".summary"), "jira.projectPage.summaryField");

        actions.waitForElementVisible("jira.projectPage.descriptionField");
        actions.typeValueInField(getConfigPropertyByKey("jira." + issueType + ".description"), "jira.projectPage.descriptionField");

        actions.waitForElementVisible("jira.projectPage.dropDownBox.priority");
        actions.clickElement("jira.projectPage.dropDownBox.priority");
        actions.clickElement("jira.projectPage.high.priority");

        actions.waitForElementVisible("jira.projectPage.createIssueButton");
        actions.clickElement("jira.projectPage.createIssueButton");

    }
}
