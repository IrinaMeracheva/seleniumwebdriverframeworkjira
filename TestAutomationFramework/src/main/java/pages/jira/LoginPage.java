package pages.jira;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BaseJiraPage{

    public LoginPage(WebDriver driver) {
        super(driver, "jira.loginUrl");
    }

    public void loginUser(String userKey){
        String username = Utils.getConfigPropertyByKey("jira.users." + userKey + ".username");
        String password = Utils.getConfigPropertyByKey("jira.users." + userKey + ".password");

        navigateToPage();
        assertPageNavigated();

        actions.waitForElementVisible("jira.loginField.username");
        actions.typeValueInField(username, "jira.loginField.username");
        actions.clickElement("jira.loginField.loginButton");

//        actions.waitFor(2000);

        actions.waitForElementVisible("jira.loginField.password");
        actions.typeValueInField(password, "jira.loginField.password");
        actions.clickElement("jira.loginField.loginButton");
    }
}
