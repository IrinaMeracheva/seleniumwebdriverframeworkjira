package pages.jira;

import org.openqa.selenium.WebDriver;

public class HomePage extends BaseJiraPage{
    public HomePage(WebDriver driver) {
        super(driver, "jira.homePage");
    }
    public void clickOnJiraSoftware(String jiraSoftware) {
        actions.waitForElementVisibleUntilTimeout("jira.mySoftwareProjects", 30, jiraSoftware);
        actions.clickElement("jira.mySoftwareProjects", jiraSoftware);
    }
}
