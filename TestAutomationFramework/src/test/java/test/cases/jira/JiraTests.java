package test.cases.jira;

import org.junit.Test;
import pages.jira.HomePage;
import pages.jira.ProjectPage;

public class JiraTests extends BaseTest{
    String jiraSoftware;

    @Test
    public void createStory() {
        login();

        HomePage homePage = new HomePage(actions.getDriver());
        homePage.clickOnJiraSoftware(jiraSoftware);

        ProjectPage projectPage = new ProjectPage(actions.getDriver());
        projectPage.navigateToPage();
        projectPage.createIssue("story");
    }
    @Test
    public void createBug() {
        login();
        HomePage homePage = new HomePage(actions.getDriver());
        homePage.clickOnJiraSoftware(jiraSoftware);

        ProjectPage projectPage = new ProjectPage(actions.getDriver());
        projectPage.navigateToPage();
        projectPage.createIssue("bug");
    }
}
