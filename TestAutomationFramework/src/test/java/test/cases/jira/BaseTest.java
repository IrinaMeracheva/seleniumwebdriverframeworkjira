package test.cases.jira;

import com.telerikacademy.testframework.UserActions;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import pages.jira.LoginPage;

public class BaseTest {

    UserActions actions = new UserActions();

    @BeforeClass
    public static void setUp() {
        UserActions.loadBrowser("jira.url");
    }

    @After
    public void tearDown() {
        UserActions.quitDriver();
    }
    public void login() {

        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser("user");
    }
}
