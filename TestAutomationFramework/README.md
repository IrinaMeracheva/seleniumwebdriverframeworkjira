**Instructions**
1. Clone repository
2. Open `aphaframework/testframework` as a IntelliJ IDEA Project
3. Go to config.properties, jira.users.user.username=enter your username,
   jira.users.user.password=your password, jira.projectUrl=your project Url
4. Build
5. Run tests in `src\test\java\testCases\`